### STAGE 1: Build ###

# We label our stage as ‘builder’
FROM node:12.16.1 as builder

#COPY package.json ./


RUN mkdir /ng-app
WORKDIR /ng-app
COPY . .
#RUN npm -v
#RUN npm cache clean --force
#RUN npm install -g @angular/cli@8.3.26
## Storing node modules on a separate layer will prevent unnecessary npm installs at each build
RUN npm install


#COPY . .

CMD ["node", "--max_old_space_size=16384", "index.js"]

## Build the angular app in production mode and store the artifacts in dist folder
RUN npm run build:prod -- --output-path=dist


### STAGE 2: Setup ###

FROM nginx:1.14.1-alpine

## Copy our default nginx config
COPY nginx/default.conf /etc/nginx/conf.d/

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

## From ‘builder’ stage copy over the artifacts in dist folder to default nginx public folder
COPY --from=builder /ng-app/dist/ng-docker-test /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
